use sakila ;
## 1- Afficher les 10 locations les plus longues (nom/prenom client, film, video club, durée)

SELECT film.title, customer.first_name, customer.last_name, 
address.address as city_store, timestampdiff(HOUR,rental_date,return_date) as JOUR
FROM rental 
JOIN inventory on rental.rental_id = inventory.inventory_id
JOIN film on inventory.inventory_id = film.film_id
JOIN customer on rental.customer_id = customer.customer_id
JOIN store on customer.store_id = store.store_id
JOIN address on store.address_id = address.address_id
JOIN city on address.address_id = city.city_id
order by timestampdiff(HOUR,rental_date,return_date) DESC LIMIT 10;

# 2- Afficher les 10 meilleurs clients actifs par montant dépensé (nom/prénom client, montant dépensé)

SELECT customer.first_name, customer.last_name,
sum(payment.amount)
FROM payment
JOIN customer on payment.customer_id = customer.customer_id
GROUP BY customer.first_name, customer.last_name
ORDER BY sum(payment.amount) DESC
LIMIT 10 ;

## 3- Afficher la durée moyenne de location par film triée de manière descendante

SELECT AVG(timestampdiff(DAY,rental_date,return_date)), film.title
FROM film 
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON rental.inventory_id = inventory.inventory_id
GROUP BY film.title
ORDER BY AVG(timestampdiff(DAY,rental_date,return_date)) DESC;

#4- Afficher tous les films ayant jamais été empruntés 

SELECT film.title FROM film
WHERE film.title NOT IN (
SELECT film.title
FROM rental 
JOIN inventory ON inventory.inventory_id = rental.inventory_id
JOIN film ON inventory.film_id = film.film_id);

#### EXEMPLE AVEC LE NOT IN
SELECT film.title
FROM film
WHERE title NOT IN ('ACADEMY DINOSAUR','ANACONDA CONFESSIONS');

#5- Afficher le nombre demployés (staff) par video club 

SELECT count(staff.staff_id), staff.store_id  FROM store
INNER join staff on staff.store_id = store.store_id 
GROUP BY staff_id;

#6- Afficher les 10 villes avec le plus de video clubs 

SELECT count(store.store_id), city FROM store 
JOIN address ON store.address_id = address.address_id
JOIN city ON city.city_id = address.city_id
GROUP BY city LIMIT 10;

#7- Afficher le film le plus long dans lequel joue Johnny Lollobrigida 

SELECT film.title, film.length , actor.first_name, actor.last_name 
FROM actor
JOIN film_actor ON actor.actor_id = film_actor.actor_id
JOIN film ON film.film_id = film_actor.film_id
WHERE (actor.first_name = 'Johnny' AND actor.last_name = 'Lollobrigida')
ORDER BY film.length DESC LIMIT 1;

#8- Afficher le temps moyen de location du film 'Academy dinosaur' 

SELECT film.title, avg(datediff(rental.return_date,rental.rental_date)) 
from film 
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
WHERE film.title = "Academy dinosaur";

#9- Afficher les films avec plus de deux exemplaires en inventaire (store id, titre du film, nombre dexemplaires) 

SELECT film.title , inventory.film_id , inventory.store_id , count(inventory.film_id)
FROM inventory 
JOIN film ON inventory.film_id = film.film_id
GROUP BY film.title,inventory.film_id , inventory.store_id
HAVING inventory.film_id > 2;
#10- Lister les films contenant 'din' dans le titre 

SELECT title 
FROM film
WHERE title LIKE "%din%";

#11- Lister les 5 films les plus empruntés 

SELECT film.title , count(rental.rental_date) 
FROM film 
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
GROUP BY film.title
ORDER BY count(rental.rental_date) DESC LIMIT 5 ;

#12- Lister les films sortis en 2003, 2005 et 2006 
SELECT title , release_year FROM film
WHERE release_year IN (2006,2005,2003);

#13- Afficher les films ayant été empruntés mais n'ayant pas encore été restitués, triés par date d'emprunt. Afficher seulement les 10 premiers. 

SELECT title , rental.rental_date, rental.return_date 
FROM film 
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
WHERE rental.return_date is null
ORDER BY rental.rental_date LIMIT 10;

#14- Afficher les films d action durant plus de 2h 

SELECT film.title , film.length, category.name 
FROM film
JOIN film_category ON film.film_id = film_category.film_id
JOIN category ON film_category.category_id = category.category_id
WHERE film.length >120 AND category.name = "Action"
ORDER BY film.length;

#15- Afficher tous les utilisateurs ayant emprunté des films avec la mention NC-17 

SELECT  DISTINCT customer.last_name , customer.first_name
FROM film
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
JOIN customer ON rental.customer_id = customer.customer_id
WHERE film.rating = "NC-17" ;

#16- Afficher les films d'animation dont la langue originale est l'anglais 

SELECT film.title , language.name , category.name 
FROM film
JOIN language ON film.language_id = language.language_id
JOIN film_category ON film.film_id = film_category.film_id
JOIN category ON film_category.category_id = category.category_id
WHERE category.name = 'animation' and language.name = 'English';

#17- Afficher les films dans lesquels une actrice nommée Jennifer a joué (bonus: en même temps qu un acteur nommé Johnny) 

SELECT film.title
FROM actor
JOIN film_actor ON actor.actor_id = film_actor.actor_id
JOIN film ON film.film_id = film_actor.film_id 
WHERE actor.first_name = 'JENNIFER' and film.title in
(SELECT film.title FROM actor
JOIN film_actor ON actor.actor_id = film_actor.actor_id
JOIN film ON film.film_id = film_actor.film_id 
WHERE actor.first_name = 'JOHNNY') ;

#18- Quelles sont les 3 catégories les plus empruntées? 

SELECT category.name , count(rental.rental_id) 
FROM film
JOIN film_category ON film.film_id = film_category.film_id
JOIN category ON film_category.category_id = category.category_id
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
GROUP BY category.name
ORDER BY count(rental.rental_id) DESC LIMIT 3;

#19- Quelles sont les 10 villes où on a fait le plus de locations? 

SELECT city.city , count(rental.rental_id)  
FROM city
JOIN address ON city.city_id = address.city_id
JOIN store ON address.address_id = store.address_id
JOIN inventory ON store.store_id = inventory.store_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
GROUP BY city.city
ORDER BY count(rental.rental_id) DESC LIMIT 10;

#20- Lister les acteurs ayant joué dans au moins 1 film

SELECT DISTINCT actor.first_name , actor.last_name FROM film
JOIN film_actor ON film.film_id = film_actor.film_id
JOIN actor ON film_actor.actor_id = actor.actor_id
GROUP BY actor.first_name , actor.last_name
HAVING count(film.film_id) >= 1;
