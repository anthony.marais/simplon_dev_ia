use sakila;
# Interrogations avancées
## 1. Afficher tout les emprunt ayant été réalisé en 2006. Le mois doit être écrit en toute lettre et le résultat doit s’afficher dans une seul colonne.
SELECT rental_id, MONTHNAME(rental_date) FROM rental WHERE year(rental_date) = 2006;
## 2. Afficher la colonne qui donne la durée de location des films en jour.
SELECT rental_id, DATEDIFF(return_date,rental_date) as JOUR FROM rental;
## 3 Afficher les emprunts réalisés avant 1h du matin en 2005. Afficher la date dans un format lisible.
SELECT rental_id , DATE_FORMAT(rental_date,"%W %M %e %Y") FROM rental WHERE CAST(rental_date AS TIME) BETWEEN "00:00:00" and "01:00:00" AND year(rental_date) = 2005;
## 4 Afficher les emprunts réalisé entre le mois d’avril et le moi de mai. La liste doit être trié du plus ancien au plus récent.
SELECT rental_id, MONTHNAME(rental_date) FROM rental WHERE extract(month from rental_date) between "04" AND "05" ORDER BY  MONTHNAME(rental_date) DESC;
## 5 Lister les film dont le nom ne commence pas par le « Le ».
SELECT film_id,title FROM film where title NOT like "Le%" OR title NOT LIKE "LE%";
## 6 Lister les films ayant la mention « PG-13 » ou « NC-17 ». Ajouter une colonne qui affichera « oui » si « NC-17 » et « non » Sinon.
SELECT title, rating ,case when rating = "NC-17" THEN "OUI" ELSE "NON" END AS resultat from film WHERE rating = "NC-17" OR rating = "PG-13"; 
## 7. Fournir la liste des catégorie qui commence par un ‘A’ ou un ‘C’. (Utiliser LEFT).
SELECT name, LEFT(name,1) FROM category WHERE name like "A%" OR name like "C%"; 
## 8. Lister les trois premiers caractères des noms des catégorie.
SELECT LEFT(name, 3) from category;
## 9. Lister les premiers acteurs en remplaçant dans leur prenom les E par des A.
SELECT REPLACE(first_name,"A","E") FROM actor ORDER BY first_name LIMIT 25;


# Les jointures

## 1. Lister les 10 premiers films ainsi que leur langues.
SELECT film.title,  language.name 
FROM film 
JOIN language ON film.film_id = language.language_id
LIMIT 10;
## 2. Afficher les film dans les quel à joué « JENNIFER DAVIS » sortie en 2006.
SELECT DISTINCT film.title, actor.first_name, actor.last_name 
FROM film
LEFT JOIN film_actor on film.film_id = film_actor.film_id
LEFT JOIN actor on film_actor.actor_id = actor.actor_id
WHERE actor.first_name = "JENNIFER" AND actor.last_name = "DAVIS";
## 3. Afficher le noms des client ayant emprunté « ALABAMA DEVIL »
SELECT film.title,customer.first_name, customer.last_name 
FROM film
LEFT JOIN inventory on film.film_id = inventory.inventory_id
LEFT JOIN rental on inventory.inventory_id = rental.rental_id
LEFT JOIN customer on rental.customer_id = customer.customer_id
WHERE film.title = "ALABAMA DEVIL";
## 4. Afficher les films louer par des personne habitant à « Woodridge ». Vérifié s’il y a des films qui n’ont jamais été emprunté.
SELECT * FROM city
WHERE city = "Woodridge";

SELECT film.title, city.city
FROM film
LEFT JOIN inventory on film.film_id = inventory.inventory_id
LEFT JOIN rental on inventory.inventory_id = rental.rental_id
LEFT JOIN customer on rental.customer_id = customer.customer_id
LEFT JOIN address on customer.address_id = address.address_id
LEFT JOIN city on address.address_id = city.city_id
WHERE customer.address_id = 8 ;

SELECT film.title, city.city
FROM film
LEFT JOIN inventory on film.film_id = inventory.inventory_id
LEFT JOIN rental on inventory.inventory_id = rental.rental_id
LEFT JOIN customer on rental.customer_id = customer.customer_id
LEFT JOIN address on customer.address_id = address.address_id
LEFT JOIN city on address.address_id = city.city_id
WHERE city.city NOT LIKE "Woodridge";

SELECT film.title, city.city
FROM film
LEFT JOIN inventory on film.film_id = inventory.inventory_id
LEFT JOIN rental on inventory.inventory_id = rental.rental_id
LEFT JOIN customer on rental.customer_id = customer.customer_id
LEFT JOIN address on customer.address_id = address.address_id
LEFT JOIN city on address.address_id = city.city_id
WHERE city.city LIKE "Woodridge";

select F.title
from film as F
join inventory as I on F.film_id = I.film_id
join rental as R on I.inventory_id = R.inventory_id
join customer as C on R.customer_id = C.customer_id
join address as A on C.address_id = A.address_id
join city as CI on A.city_id = CI.city_id
where city = 'Woodridge'

UNION 

select F.title
from film as F
join inventory as I on F.film_id = I.film_id
left join rental as R on I.inventory_id = R.inventory_id
where R.rental_id IS NULL;

# 5. Quel sont les 10 films dont la durée d’emprunt à été la plus courte ?
SELECT film.title, TIMEDIFF(return_date,rental_date) as TIME 
FROM film
LEFT JOIN inventory on film.film_id = inventory.inventory_id
LEFT JOIN rental on inventory.inventory_id = rental.rental_id
ORDER BY TIME ASC LIMIT 10 ;

## 6. Lister les films de la catégorie « Action » ordonnés par ordre alphabétique.

SELECT film.title , category.name 
FROM film
LEFT JOIN film_category on film.film_id = film_category.film_id
LEFT JOIN category on film_category.category_id = category.category_id
WHERE category.name = "Action"
ORDER BY category.name;

## 7. Quel sont les films dont la duré d’emprunt à été inférieur à 2 jour ?

SELECT film.title , timestampdiff(HOUR,rental_date,return_date) as HEURE
FROM film
LEFT JOIN inventory on film.film_id = inventory.inventory_id
LEFT JOIN rental on inventory.inventory_id = rental.rental_id
WHERE rental.return_date IS NOT NULL AND timestampdiff(HOUR,rental_date,return_date) < 48
ORDER BY timestampdiff(HOUR,rental_date,return_date);
