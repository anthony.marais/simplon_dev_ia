import streamlit as st
import pandas as pd
import os
import base64
from bokeh.io import output_file, show
from bokeh.layouts import column
from bokeh.layouts import layout
from bokeh.plotting import figure
from bokeh.models import Toggle, BoxAnnotation
from bokeh.models import Panel, Tabs
from bokeh.palettes import Set3
import time


import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from pandas.errors import ParserError
from sklearn.linear_model import SGDRegressor
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score


class Predictor:   

    def file_selector(self):
        file = st.sidebar.file_uploader("Choose a CSV file", type="csv")
        if file is not None:
            data = pd.read_csv(file)
            return data
        else:
            st.text("Please upload a csv file")


    def set_features(self):
        self.features = st.multiselect('Please choose the features including target variable that go into the model', self.data.columns )


    def prepare_data(self, split_data, train_test):
        data = self.data[self.features]
        # Standardize the feature data
        X = data.loc[:, data.columns != self.chosen_target]
      
                
        X.columns = data.loc[:, data.columns != self.chosen_target].columns
        
        y = data[self.chosen_target]

        # Train test split
        try:
            self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(X, y, test_size=(1 - train_test/100), random_state=42)
            self.X_valid, self.X_test, self.y_valid, self.y_test = train_test_split(self.X_test, self.y_test, test_size=0.5)
        except:
            st.markdown('<span style="color:red">With this amount of data and split size the train data will have no records, <br /> Please change reduce and split parameter <br /> </span>', unsafe_allow_html=True)  

    # Classifier type and algorithm selection 
    def set_classifier_properties(self):
        self.type = st.sidebar.selectbox("Algorithm type", ("Regression"))
        if self.type == "Regression":
            self.chosen_classifier = st.sidebar.selectbox("Please choose a classifier", ('Linear Regression')) 
         
    def predict(self, predict_btn):    

        if self.type == "Regression":    
            if self.chosen_classifier=='Linear Regression':
                self.alg = LinearRegression()
                self.model = self.alg.fit(self.X_train, self.y_train)
                predictions = self.alg.predict(self.X_test)
                self.predictions_train = self.alg.predict(self.X_train)
                self.predictions = predictions
                result = pd.DataFrame(columns=['Actual', 'Actual_Train', 'Prediction', 'Prediction_Train'])
        
        
        result_train = pd.DataFrame(columns=['Actual_Train', 'Prediction_Train'])
        result['Actual'] = self.y_test
        result_train['Actual_Train'] = self.y_train
        result['Prediction'] = self.predictions
        result_train['Prediction_Train'] = self.predictions_train
        result.sort_index()
        self.result = result
        self.result_train = result_train

        return self.predictions, self.predictions_train, self.result, self.result_train




        





    def cor_data(self):
        correlation = self.data.corr()
        fig, ax = plt.subplots()
        sns.heatmap(correlation, annot=True, linewidths=1.0)
        st.pyplot(fig)
    
    def pairplot(self):
        sns.set(style="white", color_codes=True)
        fig = sns.pairplot(data=self.data)
        st.pyplot(fig)





if __name__ == '__main__':
    controller = Predictor()
    try:
        controller.data = controller.file_selector()
        if controller.data is not None:
            split_data = st.sidebar.slider('Randomly reduce data size %', 1, 100, 10 )
            train_test = st.sidebar.slider('Train-test split %', 1, 99, 66 )

        controller.set_features()


        if len(controller.features) > 1:
            controller.prepare_data(split_data, train_test)
            controller.set_classifier_properties()
            predict_btn = st.sidebar.button('Predict')  
    except (AttributeError, ParserError, KeyError) as e:
        st.markdown('<span style="color:blue">WRONG FILE TYPE</span>', unsafe_allow_html=True)  



    
    if st.checkbox("Seaborn correlation",value=False):
        controller.cor_data()
    
    if st.checkbox("Seaborn pairlot",value=False):
        controller.pairplot()




    if controller.data is not None:
        if st.sidebar.checkbox('Show raw data'):
            st.subheader('Raw data')
            st.write(controller.data)