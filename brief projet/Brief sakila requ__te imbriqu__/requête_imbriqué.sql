use sakila;


# Avec une requête imbriquée sélectionner 
#tout les acteurs ayant joués dans les films ou a joué «MCCONAUGHEY CARY»
SELECT DISTINCT actor.first_name, actor.last_name
FROM film
JOIN film_actor on film.film_id = film_actor.film_id
JOIN actor on film_actor.actor_id = actor.actor_id
WHERE film.film_id  
IN 

    (SELECT film.film_id
    FROM film
    JOIN film_actor on film.film_id = film_actor.film_id
    JOIN actor on film_actor.actor_id = actor.actor_id 
    WHERE concat(actor.last_name, " " ,actor.first_name) = "MCCONAUGHEY CARY" )
     AND 
    concat(actor.last_name, " " ,actor.first_name) <> "MCCONAUGHEY CARY" ;


# Afficher tout les acteurs 
# n’ayant pas joués dans les films ou a joué «MCCONAUGHEY CARY».

SELECT DISTINCT actor.first_name, actor.last_name
FROM film
JOIN film_actor on film.film_id = film_actor.film_id
JOIN actor on film_actor.actor_id = actor.actor_id
WHERE film.title
NOT IN 

    (SELECT film.title
    FROM film
    JOIN film_actor on film.film_id = film_actor.film_id
    JOIN actor on film_actor.actor_id = actor.actor_id 
    WHERE actor.first_name = "CARY" AND actor.last_name = "MCCONAUGHEY" ;







# Afficher tout les acteurs 
# n’ayant pas joués dans les films ou a joué «MCCONAUGHEY CARY».

SELECT DISTINCT actor.first_name, actor.last_name
    FROM film
    JOIN film_actor on film.film_id = film_actor.film_id
    JOIN actor on film_actor.actor_id = actor.actor_id 
    WHERE actor.actor_id
    NOT IN 
        (
        SELECT actor.actor_id
        FROM film
        JOIN film_actor on film.film_id = film_actor.film_id
        JOIN actor on film_actor.actor_id = actor.actor_id
        WHERE actor.first_name = "CARY" AND actor.last_name =  "MCCONAUGHEY"  
        );



SELECT DISTINCT actor.actor_id, actor.first_name, actor.last_name 
FROM actor                                                              -- SELECTION des actor qui ne sont pas == a l'actor CARY n'y est pas
                                                                         -- select de tout les actor qui n'ont pas de cary mccnguey
                                                                                -- mais qui ne sont pas dans la liste de tout les actor ayant jouer dans des film ADD 
                                                                                -- 
WHERE actor.actor_id != (
    
    SELECT actor_id 
    FROM actor WHERE actor.first_name = "CARY" AND actor.last_name = "MCCONAUGHEY")     -- 
    AND actor.actor_id 
    NOT IN (                                                                                    -- mais qui ne sont pas dans 
        
        SELECT DISTINCT actor.actor_id 
        FROM actor
        JOIN film_actor ON film_actor.actor_id = actor.actor_id
        WHERE actor.actor_id != (
            
            SELECT actor_id 
            FROM actor WHERE actor.first_name = "CARY" AND actor.last_name = "MCCONAUGHEY") 
                AND film_actor.film_id IN (
                    
                    SELECT film.film_id 
                    FROM film
                    JOIN film_actor ON film_actor.film_id = film.film_id
                    JOIN actor ON actor.actor_id = film_actor.actor_id
                    WHERE actor.first_name = "CARY" AND actor.last_name = "MCCONAUGHEY")
);








SELECT DISTINCT a.first_name, a.last_name
FROM actor as a
WHERE (concat(a.first_name," ", a.last_name) != "CARY MCCONAUGHEY")             -- select de tout les actor qui n'ont pas de cary mccnguey
                                                                                -- mais qui ne sont pas dans la liste de tout les actor ayant jouer dans des film ADD 
                                                                                -- 
AND a.actor_id NOT IN (
    
    SELECT DISTINCT a.actor_id
    FROM actor as a
    JOIN film_actor as fa
        ON a.actor_id = fa.actor_id                                          -- 
    JOIN film as f
        ON fa.film_id = f.film_id
    WHERE (concat(a.first_name," ", a.last_name) != "CARY MCCONAUGHEY")      -- tout les actor_id qui ne s'appel pas CARY MCCGNEY mais qui ont jouer dans ses film
    AND 
    f.title IN (
        SELECT f.title
        FROM film as f
        JOIN film_actor as fa
            ON f.film_id = fa.film_id                                       --- select de tout les film avec cary mccogney
            ON fa.actor_id = a.actor_id
        WHERE (concat(a.first_name," ", a.last_name) = "CARY MCCONAUGHEY")
)
);




SELECT DISTINCT(concat(a.first_name, ' ', a.last_name)) as nom
FROM actor a
WHERE a.actor_id NOT IN (
    SELECT fa.actor_id
    FROM film_actor fa
    INNER JOIN actor a USING(actor_id)
    WHERE fa.film_id IN (
        SELECT film_id 
        FROM film_actor 
        INNER JOIN actor USING(actor_id) 
        WHERE concat(upper(first_name), ' ', upper(last_name)) = 'CARY MCCONAUGHEY'
    )
);



# Afficher Uniquement le nom du film qui contient le plus d 
#acteur et le nombre d acteurs associé sans utiliser L IMIT (II niveaux de sous requêtes).



SELECT count(a.actor_id) as actors_number, f.title
FROM actor as a
JOIN film_actor as fa on a.actor_id = fa.actor_id
JOIN film as f on f.film_id = fa.film_id
GROUP BY f.title
HAVING COUNT(a.actor_id) = (SELECT MAX(AN2.actors_number)
                            FROM ( SELECT count(a2.actor_id) as actors_number, f2.title
                                    FROM actor as a2
                                    JOIN film_actor as fa2 on a2.actor_id = fa2.actor_id
                                    JOIN film as f2 ON f2.film_id = fa2.film_id
                                    GROUP BY f2.title) as AN2);



# Afficher les acteurs ayant joué uniquement dans  films d’actions (Utiliser ).


    




SELECT *
from actor as a2
WHERE NOT EXISTS (
    select distinct a.actor_id
    FROM actor as a
    INNER JOIN film_actor as fa on a.actor_id = fa.actor_id
    INNER JOIN film as f ON f.film_id = fa.film_id
    INNER JOIN film_category as fc ON f.film_id = fc.film_id
    INNER JOIN category as c ON c.category_id = fc.category_id
    where c.name <> 'Action'
    and a2.actor_id = a.actor_id )
;





CREATE VIEW roles AS
SELECT actor.actor_id, actor.first_name, actor.last_name, film.film_id, film.title, film.rating, film.language_id
FROM actor
JOIN film_actor on actor.actor_id = film_actor.actor_id
JOIN film ON film.film_id = film_actor.film_id;

SELECT * FROM roles;

SELECT roles *, category.name
FROM roles
JOIN film_category on roles.film_id = film_category.film_id
JOIN category on category.category_id = film_category.category_id
;