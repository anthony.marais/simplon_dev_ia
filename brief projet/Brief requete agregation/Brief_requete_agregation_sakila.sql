USE sakila;

## 1.Afficher le nombre de films dans les quels à joué l'acteur «JOHNNY LOLLOBRIGIDA», regroupé par catégorie.

SELECT count(film.film_id), actor.first_name, actor.last_name , category.name
FROM film
JOIN film_actor ON film.film_id = film_actor.film_id
JOIN actor ON film_actor.actor_id = actor.actor_id
JOIN film_category ON film.film_id = film_category.film_id
JOIN category ON film_category.category_id = category.category_id
WHERE (actor.first_name = 'Johnny' AND actor.last_name = 'Lollobrigida')
GROUP BY category.name ,actor.first_name, actor.last_name
ORDER BY count(film.film_id) DESC;


## 2.Ecrire la requête qui affiche les catégories dans les quels «JOHNNY LOLLOBRIGIDA» totalise plus de 3 films.

SELECT count(film.film_id), actor.first_name, actor.last_name , category.name
FROM film
JOIN film_actor ON film.film_id = film_actor.film_id
JOIN actor ON film_actor.actor_id = actor.actor_id
JOIN film_category ON film.film_id = film_category.film_id
JOIN category ON film_category.category_id = category.category_id
WHERE (actor.first_name = 'Johnny' AND actor.last_name = 'Lollobrigida') 
GROUP BY category.name, actor.first_name, actor.last_name
HAVING count(film.title) > 3
ORDER BY count(film.film_id) DESC;

## 3.Afficher la durée moyenne d'emprunt des films par acteurs.

SELECT AVG(timestampdiff(DAY,rental.rental_date,rental.return_date)) , actor.first_name, actor.last_name  
FROM actor
JOIN film_actor ON actor.actor_id = film_actor.actor_id
JOIN film ON film_actor.film_id = film.film_id
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
GROUP BY actor.first_name, actor.last_name 
ORDER BY AVG(timestampdiff(DAY,rental.rental_date,rental.return_date)) DESC;


SELECT AVG(DATEDIFF(rental.return_date,rental.rental_date)) , actor.first_name, actor.last_name  
FROM actor
JOIN film_actor ON actor.actor_id = film_actor.actor_id
JOIN film ON film_actor.film_id = film.film_id
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
GROUP BY actor.first_name, actor.last_name 
ORDER BY AVG(DATEDIFF(rental.return_date,rental.rental_date)) DESC;


# 4.L'argent total dépensé au vidéos club par chaque clients, classé par ordre décroissant.

SELECT sum(amount), customer.first_name, customer.last_name, city.city as city_video_club
FROM payment
JOIN customer on payment.customer_id = customer.customer_id
JOIN store on customer.store_id = store.store_id
JOIN address on store.address_id = address.address_id
JOIN city on address.city_id = city.city_id
GROUP BY  customer.first_name, customer.last_name, city.city # customer_d
ORDER BY sum(amount) DESC;

SELECT first_name, last_name, sum(p.amount) as total
FROM customer as c 
    join rental as r on c.customer_id = r.customer_id
    join payment as p on p.rental_id = r.rental_id
GROUP BY  first_name, last_name
ORDER BY total DESC;


SELECT UNIX_TIMESTAMP(NOW(),"%Y-%m-%d");
