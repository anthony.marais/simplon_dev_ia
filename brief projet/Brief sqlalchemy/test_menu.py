# Import the necessary packages
from consolemenu import *
from consolemenu.items import *
from sqlalchemy import create_engine
import json
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import time


## IMPORT DU CONFIG.JSON
# assignation de la config.json à fichierConfig
fichierConfig = "config.json"
# ouverture et chargement des donnée contenu dans fichierConfig
with open(fichierConfig) as fichier:
    config = json.load(fichier)["mysql"]



class SqlORM():
    def __init__(self,config):
        self.config = config
        self.connector = self._connect_db()
    def _connect_db(self):
        connector = create_engine('mysql+' + config["connector"] + '://' + config["user"] + ":" + config["password"] + "@" + config["host"] + ":" + config["port"] + "/" + config["bdd"], echo=False)
        return connector





# Create the menu
menu = ConsoleMenu("PYTHON APP", "Welcome in my APP")

# Create some items

# MenuItem is the base class for all items, it doesn't do anything when selected
menu_item = MenuItem("Menu 1.")

def test_item():
    testclass = SqlORM(config)
    connection = testclass.connector
    query = pd.read_sql_query("SELECT * FROM actor",connection)
    print(query)
    input("Taper pour continuer...")

# A FunctionItem runs a Python function when selected
function_item = FunctionItem("Call a Python function", test_item)



# A CommandItem runs a console command
command_item = CommandItem("Run a console command",  "python brief_sql_alchemy.py")

# A SelectionMenu constructs a menu from a list of strings
selection_menu = SelectionMenu(["item1", "item2", "item3"])

# A SubmenuItem lets you add a menu (the selection_menu above, for example)
# as a submenu of another menu
submenu_item = SubmenuItem("Submenu item", selection_menu, menu)

mon_menu_item = MenuItem("Menu 6")
# Once we're done creating them, we just add the items to the menu
menu.append_item(menu_item)
menu.append_item(function_item)
menu.append_item(command_item)
menu.append_item(submenu_item)
menu.append_item(mon_menu_item)

# Finally, we call show to show the menu and allow the user to interact
menu.show()


