CREATE DATABASE SIMPLON;

USE SIMPLON;



SELECT nom, console FROM jeux_video; ## 1

SELECT nom FROM jeux_video WHERE console = "SuperNES" ORDER BY nom; ## 2 

SELECT possesseur FROM jeux_video WHERE nom = "Street Fighter 2" ;## 3

SELECT nom, possesseur, prix FROM jeux_video ORDER BY prix ASC LIMIT 4; ## 4

SELECT nom, possesseur FROM jeux_video WHERE possesseur LIKE "%o%"; ## 5

SELECT nom, console FROM jeux_video WHERE console = "PC" AND nbre_joueurs_max BETWEEN 4 AND 12; ## 6

SELECT distinct console FROM jeux_video ;  ## 7

SELECT DISTINCT console FROM jeux_video WHERE prix < 15; ## 8

# ou ?

SELECT DISTINCT console, MAX(prix) FROM jeux_video GROUP BY console having MAX(prix) < 15;

SELECT DISTINCT console, nbre_joueurs_max FROM jeux_video WHERE nbre_joueurs_max = 4; ## 9

SELECT nom, possesseur FROM jeux_video WHERE nom LIKE "B%" OR nom LIKE "F%" and possesseur LIKE "%e%"; # 10

SELECT nom,possesseur FROM jeux_video WHERE possesseur LIKE '%e%' AND ( nom LIKE 'B%' or nom LIKE 'F%') ; # 10 
