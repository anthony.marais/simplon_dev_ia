#!/usr/bin/env bash

echo "Waiting for MySQL..."

while ! nc -z db 3306; do
  sleep 0.5
done

echo "MySQL started"



export FLASK_APP=run.py
export FLASK_CONFIG=development


flask db init
flask db migrate
flask db upgrade




cd /Docker-Flask
python run.py

