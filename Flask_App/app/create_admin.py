from app.models import Employee
import click
from flask.cli import with_appcontext
from .db import db


@click.command("init-db")
@with_appcontext
def init_db_command():
    """Adds first data for the database"""
    
    admin = Employee(email="admin@admin.com",username="admin",password="admin",is_admin=True)
    db.session.add(admin)
    db.session.commit()
        
  
    click.echo("Initialized admin.")

